(* ::Package:: *)

(* ::Title:: *)
(*Evaluation of the 6-gluon one-loop helicity amplitudes  on a phase-space point*)


(* ::Text:: *)
(*Definition of the phase-space point. A different point can be chosen, as long as on-shell conditions and momentum conservation are preserved.*)


 p1={-3,3/Sqrt[2],3/(2 Sqrt[2]),(3 Sqrt[3/2])/2};
 p2={-3,-(3/Sqrt[2]),-(3/(2 Sqrt[2])),-((3 Sqrt[3/2])/2)};
 p3={2,2,0,0};
 p4={6/7,-(6/19),(12 Sqrt[78])/133,0};
 p5={1,-(7/38),Sqrt[78]/19,Sqrt[3]/2};
 p6={15/7,-(3/2),-(Sqrt[78]/7),-(Sqrt[3]/2)}; 


(* ::Text:: *)
(*Definition of holomorphic and anti-holomorphic spinors, together with some of their possible contractions.*)


WeylSpinor[p_]:={Sqrt[p[[1]]+p[[4]]],((p[[1]]-p[[4]]) Sqrt[p[[1]]+p[[4]]])/(p[[2]]-I p[[3]])}
DualWeylSpinor[p_]:={-((p[[2]]-I p[[3]])/Sqrt[p[[1]]+p[[4]]]),Sqrt[p[[1]]+p[[4]]]}
angle[i_,j_]:=WeylSpinor[i].LeviCivitaTensor[2].WeylSpinor[j]
square[i_,j_]:=DualWeylSpinor[j].LeviCivitaTensor[2].DualWeylSpinor[i]
mandelstamS[i_,j_]:=angle[i,j]*square[j,i]
trp[i_,j_,k_,l_]:=square[i,j]*angle[j,k]*square[k,l]*angle[l,i]


(* ::Text:: *)
(*Definition the numerical substitution rules  for the momentum-twistor variables*)


SpinorsToMomentumTwistors=N[{mandelstamS[p1,p2],-angle[p4,p3]*square[p3,p2]/(angle[p4,p1]*square[p1,p2]),-(angle[p5,p3]*square[p3,p2]+angle[p5,p4]*square[p4,p2])/(angle[p5,p1]*square[p1,p2]),
(-trp[p1,p2,p3,p4]*(trp[p2,p3,p5,p1]+trp[p2,p4,p5,p1])+mandelstamS[p1,p2]*mandelstamS[p3,p4]*(trp[p5,p1,p2,p3]+trp[p5,p1,p2,p4]+trp[p5,p1,p3,p2]+trp[p5,p1,p3,p4]))/(
 mandelstamS[p1,p2] mandelstamS[p1,p5] trp[p1,p2,p3,p4]) ,
-(angle[p4,p2]*square[p2,p1]*angle[p1,p3]+angle[p4,p3]*square[p3,p1]*angle[p1,p3])/(angle[p4,p1]*square[p1,p2]*angle[p2,p3]),
  (angle[p1,p3] angle[p2,p4])/(angle[p1,p4] angle[p2,p3]),(angle[p1,p3] angle[p2,p5])/(angle[p1,p5] angle[p2,p3]),(angle[p1,p3] angle[p2,p6])/(angle[p1,p6] angle[p2,p3])}];
numSubs=Thread[Rule[{s[1,2],y[1],y[2],y[3],y[4],se[1],se[2],se[3]}, SpinorsToMomentumTwistors]]


(* ::Text:: *)
(*Compute the numerical values of the phases at this specific phase-space point.*)


phaseNum["--++++"]=N[angle[p1,p2]^4/(angle[p2,p3]*angle[p3,p4]*angle[p4,p5]*angle[p5,p6]*angle[p6,p1])]
phaseNum["-+-+++"]=N[angle[p1,p3]^4/(angle[p2,p3]*angle[p3,p4]*angle[p4,p5]*angle[p5,p6]*angle[p6,p1])]
phaseNum["-++-++"]=N[angle[p1,p4]^4/(angle[p2,p3]*angle[p3,p4]*angle[p4,p5]*angle[p5,p6]*angle[p6,p1])]


(* ::Text:: *)
(*Define the expression of the phases in momentum-twistor variables.*)


phaseMT["--++++"]=-(s[1,2]^4/((-1+se[1]) (se[1]-se[2]) (se[2]-se[3])));
phaseMT["-+-+++"]=-(s[1,2]^4/((-1+se[1]) (se[1]-se[2]) (se[2]-se[3])));
phaseMT["-++-++"]=-(s[1,2]^4/((-1+se[1]) (se[1]-se[2]) (se[2]-se[3])));


(* ::Text:: *)
(*We evaluate the helicity amplitudes at this phase-space point. Notice that we need to divide by the phase in momentum-twistor variables and multiply back by the correct numerical phase in order to obtain a physically meaningful quantity.*)


(* ::Subsection:: *)
(*--++++*)


hel="--++++";


ampN4[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N4_--++++.m"];
((ampN4[hel][[1]]/phaseMT[hel]).ampN4[hel][[2]])*phaseNum[hel] /.numSubs


ampN1[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N1_--++++.m"];
((ampN1[hel][[1]]/phaseMT[hel]).ampN1[hel][[2]])*phaseNum[hel] /.numSubs


ampN0[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N0_--++++.m"];
((ampN0[hel][[1]]/phaseMT[hel]).ampN0[hel][[2]])*phaseNum[hel] /.numSubs


(* ::Subsection:: *)
(*-+-+++*)


hel="-+-+++";


ampN4[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N4_-+-+++.m"];
((ampN4[hel][[1]]/phaseMT[hel]).ampN4[hel][[2]])*phaseNum[hel] /.numSubs


ampN1[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N1_-+-+++.m"];
((ampN1[hel][[1]]/phaseMT[hel]).ampN1[hel][[2]])*phaseNum[hel] /.numSubs


ampN0[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N0_-+-+++.m"];
((ampN0[hel][[1]]/phaseMT[hel]).ampN0[hel][[2]])*phaseNum[hel] /.numSubs


(* ::Subsection:: *)
(*-++-++*)


hel="-++-++";


ampN4[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N4_-++-++.m"];
((ampN4[hel][[1]]/phaseMT[hel]).ampN4[hel][[2]])*phaseNum[hel] /.numSubs


ampN1[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N1_-++-++.m"];
((ampN1[hel][[1]]/phaseMT[hel]).ampN1[hel][[2]])*phaseNum[hel] /.numSubs


ampN0[hel]=Get["amplitudes_D-dimensions/amp_6g_1Loop_N0_-++-++.m"];
((ampN0[hel][[1]]/phaseMT[hel]).ampN0[hel][[2]])*phaseNum[hel] /.numSubs
