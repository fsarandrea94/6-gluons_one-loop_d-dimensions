# 6-gluons_one-loop_d-dimensions



This repository contains the expressions of the set of independent Maximally-Helicity-Violating six-gluon one-loop amplitudes. The expressions have the form of linear combinations of scalar one-loop integrals in D-dimensions, each of which is multiplied by a rational coefficient. \
The definition of the scalar integrals can be found in the file Integrals.pdf.
The coefficients are rational functions of momentum-twistor variables. Details of the parametrisation can be found in the file Momentum_Twistor.pdf. \
The organisaiton of the directory is the following
- **amplitudes_D**-dimensions contains the expressions of the nine sub-amplitudes in D-dimensions.
- **amplitudes_D**-dimensions contains the expressions of the nine sub-amplitudes expanded around D=4-2*eps, up to order O(eps^0). The integral basis is expressed in terms of special functions. 

The expressions can be evaluated on a specific numerical phase-space point in the Mathematica file Evaluations.wl. The numerical point can be changed to the user's preference.
